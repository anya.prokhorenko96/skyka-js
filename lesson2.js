//1. Дано: строка "12345". Нужно преобразовать в 12345,000
    const a = 12345;
    console.log (a.toFixed(3));
//2. Дано: строка "9876.11". Преобразовать в целое число и число с точкой.
const b = "9876.11";
console.log (parseInt(b));
console.log (parseFloat(b))

//3. Дано: несколько значений. Определить, является ли значение числом. 
//- 12345
//- "12345"
// 2e-10
    const a1 = 12345;
    const a2 = "12345";
    const a3 = 2e-10;
    console.log(Number.isFinite(12345));
    console.log(Number.isFinite("12345"));
    console.log(Number.isFinite(2e-10));

//4. Дано: несколько операций. Проверить, где результат будет числом или строкой
//- 1 + Math.PI
//- "1a" - 1
//- 77 + "2e-10"
    const b1 = 1 + Math.PI;
    const b2 = "1a" - 1;
    const b3 = 77 + "2e-10";
    console.log(typeof(b1));
    console.log(typeof(b2));
    console.log(typeof(b3));
    
//5. Дано: число 100.499. Округлить к большему, к меньшему и по правилам математики.
    const c1 = 100.499;
    console.log(Math.ceil(100.499));
    console.log(Math.round(100.499));
    console.log(Math.floor(100.499));

//6. Вывести в консоль случайное целое число от 0 до 1 (включительно)
    const w3 = (Math.random());
    console.log(Math.round(w3));

//7. Дано: число 19234. Преобразовать к строке и добавить в начало и конек строки слово "cat".
    const q = 19234;
    const q1 = String(q);
    const q2 = "cat";
    const q3 = q2+q1+q2;
    console.log (q3);
//8. Даны числа: все свойства из класса Number. Вывести максимальное и минимальное значение из них.
    console.log(Math.max(Number.EPSILON,Number.MAX_SAFE_INTEGER,Number.MAX_VALUE,Number.MIN_SAFE_INTEGER,Number.MIN_VALUE,Number.NaN,Number.NEGATIVE_INFINITY,Number.POSITIVE_INFINITY));