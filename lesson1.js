//1. Дана строка 'js'. Сделайте из нее строку 'JS'.
const str1 = "js";
console.log(str1.toUpperCase());
//2. Дана строка 'JS'. Сделайте из нее строку 'js'.
const str2 = "JS";
console.log(str2.toLowerCase());
//3. Дана строка 'я учу javascript!'. Найдите количество символов в этой строке.
const str3 = "я учу javascript!";
console.log(str3.length);
//4. Дана строка 'я учу javascript!'. Вырежите из нее слово 'учу' и слово 'javascript'.
const str4 = "я учу javascript!";
console.log(str4.slice(2, 5));
console.log(str4.slice(0, 2));
console.log(str4.slice(6, 15));
//5. Дана строка 'я учу javascript!'. Найдите позицию подстроки 'учу'.
const str5 = "я учу javascript!";
console.log(str5.indexOf('учу'));
//6. Дана строка 'Я-учу-javascript!'. Замените все дефисы на '!'
const str6 = "Я-учу-javascript!";
console.log(str6.replaceAll('-','!'));
//7. Дана строка 'я учу javascript!'. Запишите каждое слово этой строки в отдельный элемент массива.
const str7 = "я учу javascript!";
console.log(str7.split(' '));
//    8. В переменной date лежит дата в формате '2025-12-31'. Преобразуйте эту дату в формат '31.12.2025'.
const date = '2025-12-31'
console.log(date.replace('2025-12-31','31.12.2025'));
//9. Преобразуйте первую букву строки в верхний регистр.
const str8 = "я учу javascript!";
console.log(str8.replace('я','Я'));
//10. Преобразуйте первую букву каждого слова строки в верхний регистр.
const str9 = "я учу javascript!";
console.log(str9.replace('я','Я').replace('у','У').replace('j','J'));